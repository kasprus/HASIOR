#ifndef NAGRODA_
#define NAGRODA_
#include <iostream>
#include "kulka.h"
#include "zegar.h"
class NieruchomaNagrodaZwykla{
protected:
	bool aktywna;
	double x;
	double y;
	double r;
	int punkty;
public:
	NieruchomaNagrodaZwykla(const double, const double, const double, const int);
	virtual const double getX();
	virtual const double getY();
	virtual const double getX() const;
	virtual const double getY() const;
	const double getR() const;
	virtual const int getPunkty() const;
	virtual std::string getOpis() const;
	//std::string getOpis() const;
	const int kolizja(Kulka *);
	bool isAktywna() const;
	virtual void reset();
};

class NieruchomaNagrodaCzasowa : public NieruchomaNagrodaZwykla{
private:
	const int punktyMaksymalne;
	const double spadekNa1000Ms;
	double procent;
	Zegar zegar;
	bool start;
public:
	NieruchomaNagrodaCzasowa(const double,const double,const double,const int,const double);
	const double getX();
	const double getY();
	const double getX() const;
	const double getY() const;
	const int getPunkty() const;
	std::string getOpis() const;
	void reset();

};
#endif
