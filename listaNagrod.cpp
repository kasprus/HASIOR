#include "listaNagrod.h"
#include <sqlite3.h>

sqlite3 ListaNagrod::*db=nullptr;

ListaNagrod::~ListaNagrod(){
	for_each(nagrodyNieruchome.begin(), nagrodyNieruchome.end(),[](NieruchomaNagrodaZwykla* a)->void {delete a;});
	}


void ListaNagrod::dodaj(NieruchomaNagrodaZwykla *a){
	nagrodyNieruchome.push_back(a);
}

const int ListaNagrod::wykonajKolizje(Kulka *kulka){
	int wynik=0;
	for (auto i=nagrodyNieruchome.begin(); i!=nagrodyNieruchome.end(); ++i){
		wynik+=(*i)->kolizja(kulka);
	}
	return wynik;
}

void ListaNagrod::zeruj(){
	for_each(nagrodyNieruchome.begin(), nagrodyNieruchome.end(),[](NieruchomaNagrodaZwykla* a)->void {a->reset();});
}

ListaNagrod::ListaNagrod(const std::string &bazaDanych, int nrPoziomu){
	sqlite3 *db;
	int open=sqlite3_open(bazaDanych.c_str(), &db);
	if(open)throw std::string("Brak polaczenia z baza danych");
	std::cout<<"gitara\n";
	std::string zapytanie="SELECT * FROM Nagrody WHERE Poziom="+std::to_string(nrPoziomu);
	sqlite3_stmt *obiektBazy;
	std::cout<<zapytanie.c_str()<<std::endl;
	int rc;
	rc=sqlite3_prepare_v2(db, zapytanie.c_str(), -1, &obiektBazy, NULL);
	if(rc!=SQLITE_OK)throw std::string("Blad zapytania");
	std::cout<<"gitara\n";
	std::string typ;
	double x=0;
	double y=0;
	double promien=0;
	int punkty=0;
	NieruchomaNagrodaZwykla *ptr;

	while((rc=sqlite3_step(obiektBazy))!=SQLITE_DONE){
		//if(rc!=SQLITE_OK)throw std::string("blad bazy danych");

		typ=std::string(reinterpret_cast<const char*>(sqlite3_column_text(obiektBazy, 2)));
		punkty=atoi(reinterpret_cast<const char*>(sqlite3_column_text(obiektBazy, 3)));
		promien=atof(reinterpret_cast<const char*>(sqlite3_column_text(obiektBazy, 4)));
		x=atof(reinterpret_cast<const char*>(sqlite3_column_text(obiektBazy, 5)));
		y=atof(reinterpret_cast<const char*>(sqlite3_column_text(obiektBazy, 6)));

		std::cout<<typ<<std::endl;
		if(typ=="czasowa"){
			ptr=new NieruchomaNagrodaCzasowa(x,y,promien, punkty, 0.0000002);
		}
		else{
			ptr= new NieruchomaNagrodaZwykla(x,y,promien, punkty);
		}
		nagrodyNieruchome.push_back(ptr);
	}
	sqlite3_finalize(obiektBazy);

	sqlite3_close(db);

//	sqlite3_close(db);
}

const std::vector<NieruchomaNagrodaZwykla*>& ListaNagrod::getVector() const{
	return nagrodyNieruchome;
}
