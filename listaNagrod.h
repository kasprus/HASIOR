#ifndef ListaNagrod_
#define ListaNagrod_
#include <vector>
#include <fstream>
#include <iostream>
#include "kulka.h"
#include "nagroda.h"
#include <algorithm>
#include <sqlite3.h>
class ListaNagrod {
private:
	std::vector <NieruchomaNagrodaZwykla*> nagrodyNieruchome;
	static sqlite3 *db;
public:
	~ListaNagrod();
	ListaNagrod(const std::string&, int nrPoziomu);
	void dodaj(NieruchomaNagrodaZwykla*);
	const int wykonajKolizje(Kulka *);
	void zeruj();
	const std::vector<NieruchomaNagrodaZwykla*>& getVector() const;
};

#endif
