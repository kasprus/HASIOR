#include "plik_gry.h"

PlikGry::PlikGry(std::string nazwa) :rozszerzenie(".txt"), folder("pliki/"){
	nazwaOgolna=nazwa;
	numerPliku=0;
	aktualny=0;
	poczatek=nullptr;
	koniec=nullptr;
}

PlikGry::~PlikGry(){
	while(poczatek!=nullptr){
		poczatek->plik.close();
		Pole *pom=poczatek;
		poczatek=poczatek->next;
		delete pom;
	}
}

std::string PlikGry::getNazwa() const{
	return folder+nazwaOgolna+std::to_string(numerPliku);
}

bool PlikGry::isEmpty() const{
	return poczatek==nullptr? true : false;
}

/*void PlikGry::nowyPlik(){
	Pole *nowy =new Pole;
	nowy->next=nullptr;
	if(koniec==nullptr){
		poczatek=koniec=nowy;
	}
	else{
		koniec->next=nowy;
		koniec=nowy;
	}
	nowy->plik.open(nazwaOgolna+std::to_string(numerPliku++)+rozszerzenie, std::ios::out);
}*/

void PlikGry::zamknijPlik(){
	if(isEmpty())return;
	if(poczatek==koniec){
		poczatek->plik.close();
		delete poczatek;
		poczatek=nullptr;
		koniec=nullptr;
	}
	else{
		Pole *pom=poczatek;
		poczatek=poczatek->next;
		pom->plik.close();
		delete pom;
	}
	++aktualny;
}

PlikGryDoZapisu::PlikGryDoZapisu(std::string nazwa) :PlikGry(nazwa){
}

void PlikGryDoZapisu::nowyPlik(){
	Pole *nowy =new Pole;
	nowy->next=nullptr;
	if(koniec==nullptr){
		poczatek=koniec=nowy;
	}
	else{
		koniec->next=nowy;
		koniec=nowy;
	}
	nowy->plik.open(folder+nazwaOgolna+std::to_string(numerPliku++)+rozszerzenie, std::ios::out);
}

PlikGryDoZapisu::~PlikGryDoZapisu(){}

PlikGryDoZapisu & PlikGryDoZapisu::operator<<(double a){
	if(isEmpty())return *this;
	poczatek->plik<<a<<" ";
	return *this;
}

std::string PlikGry::getAktualnaNazwa() const{
	return folder+nazwaOgolna+std::to_string(aktualny);
}

PlikGryDoOdczytu::PlikGryDoOdczytu(std::string nazwa):PlikGry(nazwa){
}

PlikGryDoOdczytu::~PlikGryDoOdczytu(){
}

void PlikGryDoOdczytu::otworzPliki(int nr){
	while(nr>0){
		Pole *nowy=new Pole;
		nowy->next=nullptr;
		if(koniec==nullptr){
			poczatek=koniec=nowy;
		}
		else{
			koniec->next=nowy;
			koniec=nowy;
		}
		nowy->plik.open(folder+nazwaOgolna+std::to_string(numerPliku++)+rozszerzenie, std::ios::in);
		--nr;
	}
}

void PlikGryDoOdczytu::ladujPoziomy(std::vector<std::reference_wrapper<Poziom>> &zbior){
	double a, b, c, d,e;
	int f,g,h;
	Przeszkoda *pom;
	int nr=0;
	while(poczatek!=nullptr){
		std::cout<<nr<<std::endl;
		if(poczatek->plik.good()){

			while(!poczatek->plik.eof()){
				a=b=c=d=e=0.0;
				f=g=h=0;
				poczatek->plik>>a;
				poczatek->plik>>b;
				poczatek->plik>>c;
				poczatek->plik>>d;
				poczatek->plik>>e;
				poczatek->plik>>f;
				poczatek->plik>>g;
				poczatek->plik>>h;
				pom=new Przeszkoda(a,b,c,d,e,f,g,h);
				if(poczatek->plik.eof())break;
				//poczatek->plik
				Poziom &ppom=zbior[nr];
				ppom.przeszkody.push_back(pom);
			}
			//zamknijPlik();
		}

		zamknijPlik();
		++nr;
	}
}
