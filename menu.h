#ifndef MENU_
#define MENU_
#include "przycisk.h"
#include "mysz.h"
#include<vector>

class Menu{
public:
	Menu(size_t, size_t);
	Przycisk <int> *getWskaznik(size_t x, size_t y);
	~Menu();
	size_t getSizeX() const;
	size_t getSizeY() const;
	int getNrPoziomu(Mysz *mysz) const;
	void odswiez(std::vector<int> &wyniki);
private:
	Przycisk <int> **tab;
	double const skala;
	size_t sizeX;
	size_t sizeY;
};

#endif
