//#include "gra.h"
#include <iostream>
#include <cstdlib>
#include <math.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <vector>
#include "zegar.h"
#include "poziom.h"
#include "gra.h"
#include "nagroda.h"
#include <fstream>

Gra::Gra(int fps) : skala(0.75), menu(8,5), pasekMocy(0, 0.6, 0.5, 0.1, 4.0){
	//poziom=new Poziom();
//	plik.open("poziomgry", std::ios::out);
	this->fps=fps;
	milis_per_fps=1000000/fps;
	al_init();
	al_install_mouse();
	al_install_keyboard();
	al_init_image_addon();
	al_init_primitives_addon();
	al_init_font_addon();
	al_init_ttf_addon();
	//poziom=new Poziom();
	int liczbaPoziomow=40;
	for(int i =0; i<liczbaPoziomow; ++i){
		std::cout<<"tworze poziom: "<<i+1<<std::endl;
		Poziom *pppom;
		zbiorPoziomow.push_back(*(pppom=new Poziom(i+1)));
	}
	wynik=0;
	poziom=nullptr;
	mysz=new Mysz(800,600);
	render=new Renderer(800,600);
	PlikGryDoOdczytu podczyt;
	podczyt.otworzPliki(40);
	podczyt.ladujPoziomy(zbiorPoziomow);
	//importujWyniki();
	miernik.rozpocznij();
	std::cout<<"proba importu\n";
	try{
		importujWyniki();
		std::cout<<"ok\n";
	}
	catch(char const* a){
		std::cout<<a<<std::endl;
		for(int ab=0; ab<40; ++ab){
			wyniki.push_back(0);
		}
	}
}

void Gra::importujWyniki(const std::string a){
	std::fstream plik(a.c_str(), std::ios::in);
	if(plik.fail())throw "nie mozna zaimportowac pliku z wynikami";
	int ab;
	while(!plik.eof()){
		plik>>ab;
		if(plik.fail()&&!plik.eof())throw "blad odczytu wynikow";
		wyniki.push_back(ab);
	}
	plik.close();
}

void Gra::zapiszWyniki(const std::string a){
	std::fstream plik(a.c_str(), std::ios::out);
	if(plik.fail())throw "nie mozna otworzyc pliku z wynikami";
	int ab=0;
	while(ab<wyniki.size()){
		plik<<wyniki[ab]<<" ";
		if(plik.fail()&&!plik.eof())throw "blad zapisu wynikow";
		++ab;
		//wyniki.push_back(ab);
	}
	plik.close();
}

double Gra::getSkala() const{
	return skala;
}

Gra::~Gra(){
	//al_destroy_display(okno);
	for(int i=zbiorPoziomow.size()-1; i>=0; --i){
		Poziom &ppom=zbiorPoziomow[i];
		delete &ppom;
		zbiorPoziomow.pop_back();
		try{
			zapiszWyniki();
		}
		catch(char const * a){
			std::cout<<a<<std::endl;
		}
	}
	delete render;
	//delete poziom;
	delete mysz;
}

ALLEGRO_KEYBOARD_STATE *Gra::getKlawiatura(){
	return &klawiatura;
}

void Gra::odczytKlawiatury(){
	al_get_keyboard_state(&klawiatura);
}

void Gra::generujLosowePrzeszkody(int ilosc){
	Przeszkoda *przeszkoda;
	while(ilosc>0){
		przeszkoda=new Przeszkoda(double(rand())/double(RAND_MAX), double(rand())/double(RAND_MAX)*skala, double(rand())/double(RAND_MAX), double(rand())/double(RAND_MAX)*skala);
		poziom->wstawPrzeszkode(przeszkoda);
		--ilosc;
	}
}

void Gra::inicjujPoziom(){
	std::cerr<<"Poczatek inicjowania poziomu\n";
	wynik=0;
	poziom->getLista().zeruj();
	while(mysz->getButton()){}
	poziom->kulka->centruj();
	setSzybkosc();
	poziom->startuj(getSzybkoscX(), getSzybkoscY());
	std::cerr<<"Koniec inicjowania poziomu \n";
}

void Gra::cyklGry(){
	std::cerr<<"Poczatek funkcji cyklGry\n";
	poziom->kolizje();
	poziom->przesun();
	wynik+=poziom->getLista().wykonajKolizje(poziom->kulka);
	std::cerr<<"Koniec funkcji cyklGry\n";
}

double Gra::getSzybkoscX() const{
	return szybkoscX;
}

double Gra::getSzybkoscY() const{
	return szybkoscY;
}

void Gra::setSzybkosc(){
	Przeszkoda *liniaKursora = new Przeszkoda(0,0,0,0);
	int poprzedni=0;
	int stan;
	int pom=0;
	double posX;
	double posY;
	while(pom!=2){
		pasekMocy.setProcent(0);
		stan=mysz->getButton();
		posX=mysz->getX();
		posY=mysz->getY();
		if(poprzedni==0&&stan){
			mysz->zaczep();
			liniaKursora->setX1(mysz->getZaczepienieX());
			liniaKursora->setY1(mysz->getZaczepienieY());
			++pom;
		}
		if(poprzedni&&stan==0){
			++pom;
			szybkoscX=(mysz->getZaczepienieX()-posX)*2.0;
			szybkoscY=(mysz->getZaczepienieY()-posY)*2.0;
			pasekMocy.setWartosc(sqrt(szybkoscX*szybkoscX+szybkoscY*szybkoscY));
			render->wyswietl(&pasekMocy);
			render->wyswietl(&(poziom->getLista()));
			render->wyswietl(wynik);
			std::cout<<szybkoscX<<" "<<szybkoscY<<std::endl;
		}
		if(stan){
			liniaKursora->setX2(posX);
			liniaKursora->setY2(posY);
			szybkoscX=(mysz->getZaczepienieX()-posX)*2.0;
			szybkoscY=(mysz->getZaczepienieY()-posY)*2.0;
			pasekMocy.setWartosc(sqrt(szybkoscX*szybkoscX+szybkoscY*szybkoscY));
			render->wyswietl(liniaKursora);
			render->wyswietl(&(poziom->getLista()));
			render->wyswietl(&pasekMocy);
			render->wyswietl(wynik);
		}
		pasekMocy.setWartosc(sqrt(szybkoscX*szybkoscX+szybkoscY*szybkoscY));
		wyswietlPrzeszkody(poziom);
		render->wyswietl(poziom->kulka);
		render->wyswietl(&(poziom->getLista()));
		render->wyswietl(&pasekMocy);
		render->wyswietl(wynik);
		render->wyswietlEkran();
		render->wyczyscBufor();
		poprzedni=stan;
	}

}

void Gra::dodajPrzeszkody(){
	poziom=new Poziom(1);
	poziom->kulka->setX(0.5);
	poziom->kulka->setY(0.5*0.75);
	std::cout<<"-----------\n";
	PlikGryDoZapisu plik=PlikGryDoZapisu("mapa");
	PlikGryDoZapisu przejazd=PlikGryDoZapisu("przejazd");
	std::cout<<"--------------\n";
	plik.nowyPlik();
	przejazd.nowyPlik();
	std::cout<<"11111111111111111\n";
	//rodzaje przeszkód
	const double wsp1=0.5;
	const double wsp2=1.0;
	const double wsp3=1.5;
	const double kolor=150;
	bool flaga=1;
	double rodzajPrzeszkody=wsp2; //domyślny rodzaj przeszkody;
	while(!al_key_down(getKlawiatura(), ALLEGRO_KEY_ESCAPE)){
		odczytKlawiatury();
		if(al_key_down(getKlawiatura(), ALLEGRO_KEY_ENTER)&&flaga){
			przejazdWzorcowy(przejazd, 0.0005);
			poziom->kulka->setX(0.5);
			poziom->kulka->setY(skala/2.0);
			plik.zamknijPlik();
			plik.nowyPlik();
			przejazd.zamknijPlik();
			przejazd.nowyPlik();
			//przejazdWzorcowy(przejazd, 0.02);
			poziom->czyscPamiec();
			flaga=0;
		}
		else if(al_key_down(getKlawiatura(), ALLEGRO_KEY_1))rodzajPrzeszkody=wsp1;
		else if(al_key_down(getKlawiatura(), ALLEGRO_KEY_2))rodzajPrzeszkody=wsp2;
		else if(al_key_down(getKlawiatura(), ALLEGRO_KEY_3))rodzajPrzeszkody=wsp3;

		if(mysz->getButton()){
			flaga=1;
			Przeszkoda *liniaKursora=new Przeszkoda(mysz->getX(), mysz->getY(), mysz->getX(), mysz->getY(), rodzajPrzeszkody, (kolor)*rodzajPrzeszkody, kolor*rodzajPrzeszkody, kolor*rodzajPrzeszkody);
			while(mysz->getButton()){
				liniaKursora->setX2(mysz->getX());
				liniaKursora->setY2(mysz->getY());
				//render->wyswietl(kulka);
				render->wyswietl(liniaKursora);
				render->wyswietl(poziom->kulka);
				wyswietlPrzeszkody(poziom);
				render->wyswietlEkran();
				render->wyczyscBufor();
			}
			plik<<liniaKursora->getX1();
			plik<<liniaKursora->getY1();
			plik<<liniaKursora->getX2();
			plik<<liniaKursora->getY2();
			plik<<liniaKursora->getWspolczynnikOdbicia();
			plik<<liniaKursora->getKolor1();
			plik<<liniaKursora->getKolor2();
			plik<<liniaKursora->getKolor3();
			poziom->przeszkody.push_back(liniaKursora);
		}
		wyswietlPrzeszkody(poziom);
		//render->wyswietlEkran();
		render->wyswietl(poziom->kulka);
		render->wyswietlEkran();
		render->wyczyscBufor();

		//przejazdWzorcowy(przejazd, 0.02);

	}
	poziom->czyscPamiec();
	delete poziom;
	poziom=nullptr;
}

void Gra::przejazdWzorcowy(PlikGryDoZapisu &plik, double odlegloscMaksymalna){
	odlegloscMaksymalna*=odlegloscMaksymalna;
	//auto kwadrat=[](double &&a)->double{return a*a;};
	auto dystans=[](double x1, double y1, double x2, double y2)->double{
		return (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
	};
	wyswietlPrzeszkody(poziom);
	render->wyswietl(poziom->kulka);
	render->wyswietlEkran();
	render->wyczyscBufor();
	inicjujPoziom();
	std::vector<Punkt*> tab;
	Punkt* pom, *temp;
	//poziom->kulka->stop();
	while(!al_key_down(getKlawiatura(), ALLEGRO_KEY_SPACE)){
		//al_rest(0.01);
		std::cout<<poziom->kulka->getX()<<" * "<<poziom->kulka->getY()<<"\n";
		odczytKlawiatury();
		if(al_key_down(getKlawiatura(), ALLEGRO_KEY_R)){
			poziom->kulka->stop();
			poziom->kulka->centruj();
			while(!tab.empty()){
				delete tab.back();
				tab.pop_back();
			}
			inicjujPoziom();
		}
		cyklGry();
		bool flaga=0;
		for(std::vector<Punkt*>::iterator it=tab.begin(); it!=tab.end(); ++it){
			//temp=*it;
			render->wyswietl((*it));
			if(dystans(poziom->kulka->getX(), poziom->kulka->getY(), (*it)->getX(), (*it)->getY())<odlegloscMaksymalna){
				//render->wyswietl((*it));
				flaga=1;
			}
		}
		wyswietlPrzeszkody(poziom);
		render->wyswietl(poziom->kulka);
		render->wyswietlEkran();
		render->wyczyscBufor();
		if(flaga==0){
			pom=new Punkt(poziom->kulka->getX(), poziom->kulka->getY());
			//plik<<pom->getX()<<pom->getY()<<pom->getR();
			tab.push_back(pom);
		}

	}
	while(!tab.empty()){
		plik<<tab.back()->getX()<<tab.back()->getY();
		delete tab.back();
		tab.pop_back();
	}
}

void Gra::wyswietlPrzeszkody(Poziom *a) const{
	for(std::list<Przeszkoda*>::iterator it=a->przeszkody.begin(); it!=a->przeszkody.end(); ++it){
		render->wyswietl(*it);
	}
}

int Gra::wlaczMenu(){
	size_t a=menu.getSizeX(), b=menu.getSizeY();
	int r=0, q=0;
	while(r<a){
		q=0;
		while(q<b){
			render->wyswietl(menu.getWskaznik(r,q));
			++q;
		}
		++r;
	}
	//al_rest(1);
	render->wyswietlEkran();
	render->wyczyscBufor();
	while(!al_key_down(getKlawiatura(), ALLEGRO_KEY_ESCAPE)){
		odczytKlawiatury();
		if(menu.getNrPoziomu(mysz)){std::cout<<menu.getNrPoziomu(mysz)<<std::endl;return menu.getNrPoziomu(mysz);}
		//break;
	}

}

void Gra::rozgrywka(){
	int r;
	while(!al_key_down(getKlawiatura(), ALLEGRO_KEY_ESCAPE)){
		odczytKlawiatury();
		r=wlaczMenu();
		if(r==0){
			break;
		}
		Poziom &pom=zbiorPoziomow[r-1];
		poziom=&pom;
		inicjujPoziom();
		//NieruchomaNagrodaCzasowa nagroda(0.2, 0.4, 0.04, 1000, 0.0000001);
		std::cerr<<"Poziom zainicjowany\n";
		while(!al_key_down(getKlawiatura(), ALLEGRO_KEY_ESCAPE)){
			odczytKlawiatury();
			wyswietlPrzeszkody(poziom);
			render->wyswietl(poziom->kulka);
			render->wyswietl(&(poziom->getLista()));
			render->wyswietl(&pasekMocy);
			render->wyswietl(wynik);
			//render->wyswietl(&nagroda);
			//nagroda.getX();
			//std::cout<<"----"<<nagroda.getPunkty()<<std::endl;
			render->wyswietlEkran();
			render->wyczyscBufor();
			cyklGry();
		}
		//al_rest(1.0);
		while(al_key_down(getKlawiatura(), ALLEGRO_KEY_ESCAPE)){
			odczytKlawiatury();
		}
		wyniki[poziom->getNr()-1]=(wynik>wyniki[poziom->getNr()-1]?wynik:wyniki[poziom->getNr()-1]);
		menu.odswiez(wyniki);
	}
}
