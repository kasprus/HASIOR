#ifndef _GRA
#define _GRA

#include<allegro5/allegro.h>
#include<allegro5/allegro_image.h>
#include <fstream>
#include <vector>
#include<functional>
#include"zegar.h"
#include "poziom.h"
#include "obiekt.h"
#include "mysz.h"
#include "renderer.h"
#include "plik_gry.h"
#include "menu.h"

//#include "pasek.h"

class Gra{
public:
	static Gra &getGra(int fps=120){
		static Gra gra(fps);
		return gra;
	}
	void inicjujPoziom();
	void cyklGry();
	void setSzybkosc();
	double getSzybkoscX() const;
	double getSzybkoscY() const;
	double getSkala() const;
	void dodajPrzeszkody();
	void generujLosowePrzeszkody(int ilosc=10);
	void przejazdWzorcowy(PlikGryDoZapisu& ,double);
	ALLEGRO_KEYBOARD_STATE *getKlawiatura();
	void odczytKlawiatury();
	int wlaczMenu();
	Poziom *poziom;//wskaznik na aktualny poziom//chwilowo globalny, bedzie lista poziomow
	~Gra();
	void wyswietlPrzeszkody(Poziom *a) const;
	void rozgrywka();
	void importujWyniki(const std::string ="pliki/wyniki.txt");
	void zapiszWyniki(const std::string = "pliki/wyniki.txt");
	//void ladujPoziom(int nr);
private:
	int wynik;
	//std::fstream plik;
	//PlikGryDoZapisu plik;
	std::vector<int>wyniki;
	PasekMocy pasekMocy;
	std::vector<std::reference_wrapper<Poziom>> zbiorPoziomow;
	PlikGryDoOdczytu czytnik;
	Menu menu;
	Renderer *render;
	double szybkoscX;
	double szybkoscY;
	Gra(int fps=30);
	const double skala;
	Mysz *mysz;
	int fps;
	int milis_per_fps;
	ALLEGRO_KEYBOARD_STATE klawiatura;
	//ALLEGRO_DISPLAY *okno;
	Zegar miernik;
	//Poziom *poziom; //bedzie lista poziomów
};

#endif
