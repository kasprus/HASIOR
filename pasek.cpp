#include<allegro5/allegro.h>
#include<allegro5/allegro_image.h>
#include<allegro5/allegro_primitives.h>
#include"pasek.h"

Pasek::Pasek(const double x, const double y, const double szerokosc, const double wysokosc):x(x), y(y), wysokosc(wysokosc), szerokosc(szerokosc){}

const std::string Pasek::getNapis() const{
	return nazwa;
}

void Pasek::setNazwa(const std::string &nazwa){
	this->nazwa=nazwa;
}

const double Pasek::getX() const{
	return x;
}

const double  Pasek::getY() const{
	return y;
}

const double Pasek::getSzerokosc() const{
	return szerokosc;
}

const double Pasek::getWysokosc() const{
	return wysokosc;
}

PasekMocy::PasekMocy(const double x, const double y, const double szerokosc, const double wysokosc, const double maximumValue, const bool poziomo): Pasek(x,y, szerokosc, wysokosc), maximumValue(maximumValue), poziomo(poziomo){
	currentValue=0;
}

void PasekMocy::setWartosc(const double &a){
	if(a>maximumValue||a<0.0)throw std::string("blad");
	currentValue=a;
}

void PasekMocy::setProcent(const double &a){
	if(a<0.0||a>100.0)throw std::string("blad");
	currentValue=a/100.0*maximumValue;
}

const double PasekMocy::getWartosc() const{
	return currentValue;
}

const double PasekMocy::getProcent() const{
	return 100.0*currentValue/maximumValue;
}

const double PasekMocy::getOgraniczoneX() const{
	if(poziomo)return getX()+getSzerokosc()*getProcent()/100.0;
	return getX()+getSzerokosc();
}

const double PasekMocy::getOgraniczoneY() const{
	if(poziomo)return getY()+getWysokosc();
	return getY()+getWysokosc()*getProcent()/100.0;
}

PasekPunktow::PasekPunktow(const double x, const double y, const double szerokosc, const double wysokosc):Pasek(x,y,szerokosc,wysokosc){
	punkty=0;
}

const unsigned int PasekPunktow::getPunkty() const{
	return punkty;
}

const std::string PasekPunktow::getNapis() const{
	return Pasek::getNapis()+" "+std::to_string(punkty);
}

void PasekPunktow::setPunkty(const unsigned int a){
	punkty=a;
}
