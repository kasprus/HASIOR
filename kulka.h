#ifndef KULKA_
#define KULKA_
/*
#include "przeszkoda.h"
#include "zegar.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <math.h>
*/
#include "przeszkoda.h"
#include "zegar.h"
#include "punkt.h"
//#include "obiekt.h"
#define _SUFIT 0
#define _SCIANA_PRAWA 1
#define _PODLOGA 2
#define _SCIANA_LEWA 3
class Kulka{
public:
	Kulka(double x=0.5, double y=0.5*0.75, double promien=1/800.0*10, int kolor1=0, int kolor2=255, int kolor3=70);
	//~Kulka();

	double getX() const;
	double getY() const;
	double getR() const;
	double getOdbicie();
	void przesun();
	void odbij(Przeszkoda &przeszkoda);
	void odbij(int sciana);
	//void wyswietl() const;
	void startuj(double vx=0.1, double vy=0.01);
	bool aktywny() const;
	void ustawPoprzednieWspolrzedne();
	void resetPoprzednie();
	double getOdleglosc(const Punkt&) const;
	void setX(double);
	void setY(double);
	void stop();
	void centruj();
	double getTempMnoznik() const;
	double getSzybkoscX() const;
	double getSzybkoscY() const;
	void setSzybkoscX(double);
	void setSzybkoscY(double);
private:
	double tempMnoznik;
	const double skala;
	bool active;
	double przyspieszenie;
	double szybkosc_x;
	double szybkosc_y;
	double x_poprzednia;
	double y_poprzednia;
	double wsp_odbicia;
	double x;
	double y;
	double promien;
	int kolor1;
	int kolor2;
	int kolor3;
	//double skala;
	Zegar miernik;

};

#endif
