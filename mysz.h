#ifndef MYSZ_
#define MYSZ_
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
class Mysz{
public:
	Mysz(int x, int y);
	~Mysz();
	double getX();
	double getY();
	double getZaczepienieX() const;
	double getZaczepienieY() const;
	double getButton();
	void zaczep();
	//void wyswietlLinie();
private:
	ALLEGRO_MOUSE_STATE stanMyszy;
	double xZaczepienia;
	double yZaczepienia;
	int rozdzielczoscX;
	int rozdzielczoscY;
	double skala;
};
#endif
