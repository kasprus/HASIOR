#include "nagroda.h"
#include <math.h>

NieruchomaNagrodaZwykla::NieruchomaNagrodaZwykla(const double x, const double y, const double r, const int punkty): x(x), y(y), r(r), punkty(punkty), aktywna(true){}

const double NieruchomaNagrodaZwykla::getX(){
	return x;
}

const double NieruchomaNagrodaZwykla::getY(){
	return y;
}

const double NieruchomaNagrodaZwykla::getX() const{
	return x;
}

const double NieruchomaNagrodaZwykla::getY() const{
	return y;
}

const double NieruchomaNagrodaZwykla::getR() const{
	return r;
}

const int NieruchomaNagrodaZwykla::getPunkty() const{
	//getX();
	return punkty;
}

std::string NieruchomaNagrodaZwykla::getOpis() const{
	return std::to_string(getPunkty());
}

const int NieruchomaNagrodaZwykla::kolizja(Kulka *kulka){
	if(!aktywna)return 0;
	double x=getX();
	double y=getY();
	double x2=x-kulka->getX();
	x2*=x2;
	double y2=y-kulka->getY();
	y2*=y2;
	std::cout<<"kolizyje_______: "<<y2+x2<<" vs "<<kulka->getR()*kulka->getR()<<std::endl;
	if(x2+y2<(kulka->getR()+getR())*(kulka->getR()+getR())){
		aktywna=false;
		return getPunkty();
	}
	return 0;
}

bool NieruchomaNagrodaZwykla::isAktywna() const{
	return aktywna;
}

void NieruchomaNagrodaZwykla::reset(){
	aktywna=true;
}

NieruchomaNagrodaCzasowa::NieruchomaNagrodaCzasowa(const double x,const double y,const double r, const int punkty, const double spadek): NieruchomaNagrodaZwykla(x,y,r,punkty),start(false), procent(0), punktyMaksymalne(punkty), spadekNa1000Ms(spadek){
}

const double NieruchomaNagrodaCzasowa::getX(){
	std::cout<<"+++++Jestemtutaj ";
	if(start){
		double pom=double(zegar.czasTrwaniaMilis());
		std::cout<<pom<<" "<<spadekNa1000Ms<<std::endl;
		procent+=pom*spadekNa1000Ms;
		procent=(procent>0.8?0.8:procent);
	}
	zegar.rozpocznij();
	start=true;
	return x;
}

const double NieruchomaNagrodaCzasowa::getY(){
	if(start){
		procent+=double(zegar.czasTrwaniaMilis())*spadekNa1000Ms;
		procent=(procent>0.8?0.8:procent);
	}
	zegar.rozpocznij();
	start=true;
	return y;
}

const double NieruchomaNagrodaCzasowa::getX() const{
	return x;
}

const double NieruchomaNagrodaCzasowa::getY() const{
	return y;
}

std::string NieruchomaNagrodaCzasowa::getOpis() const{
	return std::to_string(getPunkty());
}

/*std::string NieruchomaNagrodaCzasowa::getOpis() const{
	//double pom=double(punktyMaksymalne)*procent;
	return std::to_string(getPunkty());
}*/

const int NieruchomaNagrodaCzasowa::getPunkty() const{
	//std::cout<<"jestem tam "<<procent<<std::endl;
	return double(punktyMaksymalne)*(1.0-procent);
}
void NieruchomaNagrodaCzasowa::reset(){
	aktywna=true;
	procent=0;
	start=false;
}
