#ifndef PUNKT_
#define PUNKT_
class Punkt{
private:
	double x;
	double y;
	double r;
public:
	Punkt(double x_, double y_, double r_=0.004);
	double getX() const;
	double getY() const;
	double getR() const;
};
#endif
