#include "menu.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include<vector>
Menu::Menu(size_t x, size_t y):skala(0.75){
	std::vector<int> wyniki;
	std::ifstream plik;
	//plik.exceptions ( ifstream::failbit | ifstream::badbit );
	try{
		plik.open("pliki/wyniki.txt");

		int pom;
		while(!plik.eof()){
			plik>>pom;
			wyniki.push_back(pom);
			std::cout<<pom<<" ";
		}

		plik.close();
	}
	catch(const std::ifstream::failure& e){
		abort();
	}

	sizeX=x;
	int a=1;
	sizeY=y;
	tab=new Przycisk<int>* [x];
	x=0;
	int pom;
	double pomX, pomY;
	auto pomX1=[](size_t x, size_t nr)-> double{
		double pom=1.0/double(x);
		return pom*0.2+double(nr)*pom;
	};

	auto pomY1=[](size_t y, size_t nr)-> double{
		double pom=1.0/double(y);
		return pom*0.2+double(nr)*pom;
	};

	auto pomX2=[](size_t x, size_t nr)-> double{
		double pom=1.0/double(x);
		return double(nr+1)*pom-0.2*pom;
	};

	auto pomY2=[](size_t y, size_t nr)-> double{
		double pom=1.0/double(y);
		return -pom*0.2+double(nr+1)*pom;
	};

	while(x<sizeX){
		tab[x]=new Przycisk <int> [y];
		pom=0;
		while(pom<y){
			tab[x][pom].setOpis(pom*sizeX+x+1);
			tab[x][pom].setRozwiniece("Rekord: "+std::to_string(pom*sizeX+x+1>=wyniki.size()?0:wyniki[pom*sizeX+x]));
			tab[x][pom].setX1(pomX1(sizeX,x));
			tab[x][pom].setX2(pomX2(sizeX,x));
			tab[x][pom].setY1(skala*pomY1(sizeY,pom));
			tab[x][pom].setY2(skala*pomY2(sizeY,pom));
			//al_rest(0.1);
			++pom;
		}
		++x;
	}
	//std::cin>>a;
}

Przycisk <int>* Menu::getWskaznik(size_t x, size_t y){
	if(x>=sizeX||y>=sizeY)return nullptr;
	return static_cast<Przycisk <int>* >(&(tab[x][y]));
}

size_t Menu::getSizeX() const{
	return sizeX;
}

size_t Menu::getSizeY() const{
	return sizeY;
}

Menu::~Menu(){
	int a=0;
	while(a<sizeX){
		delete [] tab[a];
		++a;
	}
}

int Menu::getNrPoziomu(Mysz *mysz) const {
	//std::cout<<"*******************\n";
	//std::cout<<sizeX<<" "<<sizeY<<std::endl;
	for(int i=0; i<sizeX; ++i){
		for(int ii=0; ii<sizeY; ++ii){
			//std::cout<<i<<" "<<ii<<std::endl;
			if((tab[i][ii].getX1()-mysz->getX())*(tab[i][ii].getX2()-mysz->getX())<0.0){
				if((tab[i][ii].getY1()-mysz->getY())*(tab[i][ii].getY2()-mysz->getY())<0.0){
					if(mysz->getButton())return atoi(tab[i][ii].getOpis().c_str());
				}
			}
		}
	}
	//std::cout<<"0\n";
	return 0;
}

void Menu::odswiez(std::vector<int> &wyniki){
	int x=0, y=0;
	for(auto elem:wyniki){
		std::cout<<"////jestemtutaj\n";
		getWskaznik(x++, y)->setRozwiniece(std::to_string(elem));
		if(x==sizeX){
			++y;
			x=0;
		}
		if(y==sizeY)break;
	}
}
