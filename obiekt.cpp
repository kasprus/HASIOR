#include"obiekt.h"

Obiekt::Obiekt(int x, int y){
	skala=double(1.0/double(x));
	rozdzielczoscX=x;
	rozdzielczoscY=y;
	stosunek =double(y)/double(x);
}

double Obiekt::getSkala() const{
	return skala;
}

int Obiekt::getRozdzielczoscX() const{
	return rozdzielczoscX;
}


int Obiekt::getRozdzielczoscY() const{
	return rozdzielczoscY;
}

double Obiekt::getStosunek() const{
	return stosunek;
}
