#ifndef PRZYCISK_
#define PRZYCISK_

#include <iostream>
#include <string>

template <class T>
class Przycisk{
private:
	std::string opis;
	std::string rozwiniecie;
	double x1;
	double y1;
	double x2;
	double y2;
public:
	Przycisk(const T&);
	Przycisk();
	//Przycisk(const std::string&);
	void setRozwiniece(const std::string&);
	const std::string getRozwiniecie() const;
	void setOpis(const T&);
	double getX1() const;
	double getX2() const;
	double getY1() const;
	double getY2() const;
	void setX1(double);
	void setX2(double);
	void setY1(double);
	void setY2(double);
	std::string getOpis() const;
	bool isActive(double, double) const;

};


#endif
