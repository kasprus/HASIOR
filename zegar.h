#ifndef ZEGAR_
#define ZEGAR_
/*
#include <chrono>
#include<iostream>
*/
#include <chrono>
class Zegar{
public:
	void rozpocznij();
	//void zakoncz();
	//void zakoncz_i_rozpicznij();
	long long int czasTrwaniaMilis() const;
private:
	std::chrono::steady_clock::time_point poczatek;
	std::chrono::steady_clock::time_point koniec;
};

#endif
