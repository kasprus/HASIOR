#include <chrono>
#include<iostream>
#include "zegar.h"

void Zegar::rozpocznij(){
	this->poczatek=std::chrono::steady_clock::now();
}

/*void zegar::zakoncz(){
	this->koniec=std::chrono::steady_clock::now();
}*/

long long int Zegar::czasTrwaniaMilis() const{
	return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-this->poczatek).count();
}
