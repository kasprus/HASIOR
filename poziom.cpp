#include <math.h>
#include<list>
#include<iostream>
#include "kulka.h"
#include "przeszkoda.h"
#include "poziom.h"

Poziom::Poziom(int nr, double kulka_x, double kulka_y):skala(0.6), listaNagrod("pliki/nagrody.db", nr){
	kulka=new Kulka(kulka_x, kulka_y);
	nrPoziomu=nr;
	//this->x=1.0;
	//this->y=double((double)y/(double)x);
}

ListaNagrod& Poziom::getLista(){
	return listaNagrod;
}

Poziom::~Poziom(){
	delete kulka;
	while(!przeszkody.empty()){
		delete przeszkody.back();
		przeszkody.pop_back();
	}
}

void Poziom::kolizje(){
	if(kulka->getY()<=kulka->getR()){
		kulka->odbij(0);
		kulka->ustawPoprzednieWspolrzedne();
		kulka->resetPoprzednie();
		return;
	}
	else if(kulka->getY()>=skala-kulka->getR()){
		kulka->odbij(2);
		kulka->ustawPoprzednieWspolrzedne();
		kulka->resetPoprzednie();
		return;
	}
	else if(kulka->getX()<=kulka->getR()){
		kulka->odbij(3);
		kulka->ustawPoprzednieWspolrzedne();
		kulka->resetPoprzednie();
		return;
	}
	else if(kulka->getX()>=1.0-kulka->getR()){
		kulka->odbij(1);
		kulka->ustawPoprzednieWspolrzedne();
		kulka->resetPoprzednie();
		return;
	}
	else{
		for(std::list<Przeszkoda*>::iterator it=przeszkody.begin(); it!=przeszkody.end(); ++it){
			if(kolizja(*it)){kulka->resetPoprzednie();return;}
		}
	}
}

double Poziom::odleglosc(double x1, double x2, double y1, double y2, double x, double y) const{
	double px=x2-x1;
	double py=y2-y1;
	double temp=px*px+py*py;
	double u=((x-x1)*px+(y-y1)*py)/temp;
	if(u>1.0)u=1.0;
	if(u<0.0)u=0.0;
	double xx=x1+u*px;
	double yy=y1+u*py;
	double dx=xx-x;
	double dy=yy-y;
	std::cout<<dx<<"..."<<dy<<" "<<sqrt(dx*dx+dy*dy)<<std::endl;
	return sqrt(dx*dx+dy*dy);
}

bool Poziom::kolizja(Przeszkoda *przeszkoda){
	double px=przeszkoda->getX2()-przeszkoda->getX1();
	double py=przeszkoda->getY2()-przeszkoda->getY1();
	double temp=px*px+py*py;
	//if(temp<0.000001||temp>100000.0)return false;
	//std::cout<<"***"<<temp<<" ";
	double u=((kulka->getX()-przeszkoda->getX1())*px+(kulka->getY()-przeszkoda->getY1())*py)/temp;
	if(u>	1.0)u=1.0;
	else if(u<0.0)u=0.0;
	//std::cout<<u<<" ";
	double xx=przeszkoda->getX1()+u*px;
	double yy=przeszkoda->getY1()+u*py;
	double dx=xx-kulka->getX();
	double dy=yy-kulka->getY();
	double dist=sqrt(dx*dx+dy*dy);
	if(dist<=kulka->getR()){
		//std::cout<<"++++"<<((kulka->getX()-przeszkoda->getX1())*px+(kulka->getY()-przeszkoda->getY1())*py)/temp<<std::endl;
		//std::cin>>xx;
		kulka->odbij(*przeszkoda);
		kulka->ustawPoprzednieWspolrzedne();
		if(odleglosc(przeszkoda->getX1(), przeszkoda->getX2(), przeszkoda->getY1(), przeszkoda->getY2(), kulka->getX()+kulka->getTempMnoznik()*kulka->getSzybkoscX(), kulka->getY()+kulka->getTempMnoznik()*kulka->getSzybkoscY())<=kulka->getR()){
			kulka->setSzybkoscX(kulka->getSzybkoscX()*(-1.0));
			kulka->setSzybkoscY(kulka->getSzybkoscY()*(-1.0));
		}
		return true;
	}
return false;
//std::cin>>dx;
}

void Poziom::przesun(){
	kulka->przesun();
}

/*void Poziom::wyswietl(){
	for(std::list<Przeszkoda*>::iterator it=przeszkody.begin(); it!=przeszkody.end(); ++it){
		(*it)->wyswietl();

	}
	kulka->wyswietl();
}*/

void Poziom::startuj(double szybkoscKulkiX, double szybkoscKulkiY){
	kulka->startuj(szybkoscKulkiX, szybkoscKulkiY);
}

void Poziom::wstawPrzeszkode(Przeszkoda *przeszkoda){
	przeszkody.push_back(przeszkoda);
}

void Poziom::czyscPamiec(){
	while(!przeszkody.empty()){
		delete przeszkody.back();
		przeszkody.pop_back();
	}
}

int Poziom::getNr() const{
	return nrPoziomu;
}
