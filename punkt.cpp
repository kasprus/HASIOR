#include "punkt.h"

Punkt::Punkt(double x_, double y_, double r_): x(x_), y(y_), r(r_){}

double Punkt::getX() const{
	return x;
}

double Punkt::getY() const{
	return y;
}

double Punkt::getR() const{
	return r;
}
