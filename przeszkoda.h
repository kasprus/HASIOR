#ifndef PRZESZKODA_
#define PRZESZKODA_
#include "obiekt.h"
/*
#include<allegro5/allegro.h>
#include<allegro5/allegro_image.h>
#include<allegro5/allegro_primitives.h>
#include<math.h>
#include<iostream>
*/
class Przeszkoda{
public:
	Przeszkoda(double x1, double y1, double x2, double y2, double wspolczynnikOdbicia=1.0, int kolor1=0, int kolor2=255, int kolor3=0);
	//~Przeszkoda();
	//double get_a();
	//double get_b();
	void setX1(double);
	void setX2(double);
	void setY1(double);
	void setY2(double);

	double getX1() const;
	double getX2() const;
	double getY1() const;
	double getY2() const;
	//void wyswietl() const;
	double getWektorNormalnyX() const;
	double getWektorNormalnyY() const;
	double getWspolczynnikOdbicia() const;
	int getKolor1() const;
	int getKolor2() const;
	int getKolor3() const;
private:
	double x1;
	double x2;
	double y1;
	double y2;
	double wspolczynnikOdbicia;
	const double skala;
	int kolor1;
	int kolor2;
	int kolor3;
	double wektor_normalny_x;
	double wektor_normalny_y;
};

#endif
