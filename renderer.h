#ifndef RENDERER_
#define RENDERER_

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <iostream>
#include "przeszkoda.h"
#include "kulka.h"
#include "punkt.h"
#include "przycisk.h"
#include "pasek.h"
#include "nagroda.h"
#include "listaNagrod.h"
//#include "pasek.h"

class Renderer{
public:
	Renderer(int x=800, int y=600, int fps=60);
	~Renderer();
	void wyswietl(const Przeszkoda *) const;
	//void wyswietl(Przeszkoda *);
	void wyswietl(const Kulka *) const;
	void wyswietl(const Punkt *) const;
	void wyswietl(const Przycisk<int> *)const;
	void wyswietl(const PasekMocy *) const;
	void wyswietl(const NieruchomaNagrodaZwykla *)const;
	void wyswietl(const ListaNagrod *)const;
	void wyswietl(int punkty) const;
	void wyczyscBufor() const;
	//void wyswietl(const Pasek *) const;
	void wyswietlEkran() const;
	void zapiszEkran(std::string nazwa) const;
private:
	int x;
	int y;
	double skala;
	ALLEGRO_FONT *czcionka;
	ALLEGRO_FONT *malaCzcionka;
	ALLEGRO_DISPLAY *okno;
};

#endif
