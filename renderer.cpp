#include "renderer.h"
#include <iostream>
#include <cstdio>
Renderer::Renderer(int x, int y, int fps){
	al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
	al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
	czcionka=al_load_ttf_font_f(al_fopen("Chewy.ttf", "r"), "Chewy", y/10, 0);
	malaCzcionka=al_load_ttf_font_f(al_fopen("Chewy.ttf", "r"), "Chewy", y/50, 0);
	if(czcionka==nullptr){
		std::cout<<"duuuuuuuuuupaaaaaa";
		exit(-1);
	}
	this->x=x;
	this->y=y;
	skala=double(y)/double(y);
	okno=al_create_display(x, y);
	//al_clear_to_color(al_map_rgb( 255, 0, 0));
	al_set_target_bitmap(al_get_backbuffer(okno));
	al_set_window_title(okno, "HASIOR");
	al_clear_to_color(al_map_rgb( 255, 255, 255));
}

Renderer::~Renderer(){
	al_destroy_display(okno);
}


void Renderer::wyswietlEkran() const{
	al_flip_display();
}

void Renderer::wyswietl(const Kulka *a) const{
	al_draw_filled_circle(a->getX()*x, a->getY()*x, a->getR()*x, al_map_rgb(60, 60, 60));
}

void Renderer::wyswietl(const PasekMocy *a) const{
	al_draw_filled_rounded_rectangle(a->getX()*x, a->getY()*x*1.05, a->getOgraniczoneX()*x, a->getOgraniczoneY()*x*1.05, (a->getOgraniczoneX()-a->getX())/25.0*x, (a->getOgraniczoneX()-a->getX())/25.0*x, al_map_rgb(255.0*a->getProcent()/100.0, 120, 160));
	al_draw_rounded_rectangle(a->getX()*x, a->getY()*x*1.05, x*(a->getX()+a->getSzerokosc()), x*(a->getY()*1.05+a->getWysokosc()), (a->getSzerokosc())/25.0*x, (a->getSzerokosc())/25.0*x, al_map_rgb(40, 66, 156), x/60.0);
	//std::cout<<"wyswietlam: "<<a->getX()*x<<" "<<a->getY()*x<<" "<<a->getOgraniczoneX()*x<<" "<<a->getOgraniczoneY()*x<<std::endl;
	std::cout<<"wyswietlam: "<<a->getProcent()<<std::endl;
}

void Renderer::wyswietl(const Przeszkoda *a) const{
	al_draw_line(a->getX1()*x, a->getY1()*x, a->getX2()*x, a->getY2()*x, al_map_rgb(a->getKolor1(),a->getKolor2(),a->getKolor3()),1);
}

void Renderer::wyswietl(const Przycisk<int> *a) const{
	al_draw_filled_rectangle(a->getX1()*x, a->getY1()*x, a->getX2()*x, a->getY2()*x, al_map_rgb(0,255,0));
	al_draw_text(czcionka, al_map_rgb(0,0,0), (a->getX1()+a->getX2())*x/2.0, a->getY1()*x, ALLEGRO_ALIGN_CENTRE, a->getOpis().c_str());
	al_draw_text(malaCzcionka, al_map_rgb(0,0,0), (a->getX1()+a->getX2())*x/2.0, a->getY1()*x, ALLEGRO_ALIGN_CENTRE, a->getRozwiniecie().c_str());
	//std::cout<<a->getRozwiniecie()<<std::endl;
}
/*void Renderer::wyswietl(Przeszkoda *a){
	al_draw_line(a->getX1()*x, a->getY1()*x, a->getX2()*x, a->getY2()*x, al_map_rgb(140,140,140),1);
}*/

void Renderer::wyswietl(const NieruchomaNagrodaZwykla *a) const{
	al_draw_filled_circle(a->getX()*x, a->getY()*x, a->getR()*x, al_map_rgb(0, 220, 10));
	al_draw_text(malaCzcionka, al_map_rgb(0,20,0), a->getX()*x, a->getY()*x, ALLEGRO_ALIGN_CENTRE, a->getOpis().c_str());
}

void Renderer::wyswietl(int punkty) const{
	al_draw_text(czcionka, al_map_rgb(0,0,0), 0.8*x, 0.8*y, ALLEGRO_ALIGN_CENTRE, std::to_string(punkty).c_str());
}

void Renderer::wyswietl(const ListaNagrod *a) const{
	std::vector<NieruchomaNagrodaZwykla*> pom=a->getVector();
	for(auto i=pom.begin(); i!=pom.end(); ++i){
		if((*i)->isAktywna())wyswietl(*i);
	}
}

void Renderer::wyczyscBufor() const{
	al_clear_to_color(al_map_rgb( 255, 255, 255));
}

void Renderer::zapiszEkran(std::string nazwa) const{
	ALLEGRO_BITMAP *bufor=al_get_backbuffer(okno);
	std::string pom=nazwa+".bmp";
	std::cout<<al_save_bitmap(pom.c_str(), al_get_backbuffer(okno));
}

void Renderer::wyswietl(const Punkt *a) const{
	al_draw_filled_circle(a->getX()*x, a->getY()*x, a->getR()*x, al_map_rgb(30,30,30));
}
