#ifndef OBIEKT_
#define OBIEKT_

class Obiekt{
public:
	Obiekt(int x=800, int y=600); //rozdzielczosc
	double getSkala() const;
	double getStosunek() const;
	int getRozdzielczoscX() const;
	int getRozdzielczoscY() const;
private:
	double skala;
	double stosunek;
  int rozdzielczoscX;
	int rozdzielczoscY;
};

#endif
