//klasa pasku pokazujacego sile, z jaka bedzie wystrzelona kulka;
#ifndef PASEK_
#define PASEK_
#include<iostream>
class Pasek{
public:
	Pasek(const double x, const double y, const double szerokosc, const double wysokosc);
	void setNazwa(const std::string &nazwa);
	virtual const std::string getNapis() const;
	const double getX() const;
	const double getY() const;
	const double getSzerokosc() const;
	const double getWysokosc() const;

private:
	std::string nazwa;
	const double szerokosc;
	const double wysokosc;
	const double x;
	const double y;
};

class PasekMocy: public Pasek{
public:
	PasekMocy(const double x, const double y, const double szerokosc, const double wysokosc, const double maximumValue, const bool poziomo=true);
	void setWartosc(const double &);
	void setProcent(const double &);
	const double getWartosc() const;
	const double getProcent() const;
	const double getOgraniczoneX() const;
	const double getOgraniczoneY() const;
private:
	const double maximumValue;
	double currentValue;
	bool poziomo;
};

class PasekPunktow: public Pasek{
public:
	PasekPunktow(const double x, const double y, const double szerokosc, const double wysokosc);
	const unsigned int getPunkty() const;
	void setPunkty(const unsigned int);
	const std::string getNapis() const;
private:
	unsigned int punkty;

};

#endif
