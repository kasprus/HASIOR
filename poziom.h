#ifndef POZIOM_
#define POZIOM_
/*
#include <math.h>
#include<list>
#include<iostream>
#include "kulka.h"
#include "przeszkoda.h"
*/
#include<functional>
#include "kulka.h"
#include "przeszkoda.h"
#include<list>
#include "obiekt.h"
#include "nagroda.h"
#include "listaNagrod.h"
class Poziom{
public:

	Poziom(int nr, double kulka_x=0.25, double kulka_y=0.2);
	~Poziom();
	void startuj(double szybkoscKulkiX=0, double szybkoscKulkiY=0);
	//void wyswietl();
	int getNr() const;
	void kolizje();
	void przesun();
	void wstawPrzeszkode(Przeszkoda *przeszkoda);
	double odleglosc(double x1, double x2, double y1, double y2, double x, double y) const;
	void czyscPamiec(); //niebezpieczne!! tylko dla dynamicznych obiektów!!
	Kulka *kulka; //chwilowo publiczny
	std::list<Przeszkoda*> przeszkody;
	void dodajNagrodeNieruchoma(NieruchomaNagrodaZwykla*);
	ListaNagrod& getLista();
private:
	bool kolizja(Przeszkoda *przeszkoda);
	const double skala;
	ListaNagrod listaNagrod;
	int nrPoziomu;
	//vector<NieruchomaNagrodaZwykla*> nagrody;
	//double x;
	//double y;
	//std::list<Przeszkoda*>przeszkody;
	//Kulka *kulka;
};

#endif
