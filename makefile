##makefile dla gry w odbijającą się piłeczkę

CC=g++
CFLAGS=
LDFLAGS= -L/usr/lib -lallegro -lallegro_image -lallegro_primitives -lallegro_font -lallegro_ttf -lsqlite3
INCLUDE= -I. -I/usr/include/allegro5

all: main.o gra.o zegar.o przeszkoda.o kulka.o poziom.o mysz.o renderer.o plik_gry.o punkt.o przycisk.o menu.o pasek.o nagroda.o listaNagrod.o
	$(CC) main.o gra.o zegar.o przeszkoda.o kulka.o poziom.o mysz.o renderer.o plik_gry.o punkt.o przycisk.o menu.o pasek.o nagroda.o listaNagrod.o -o gra $(INCLUDE) $(CFLAGS) $(LDFLAGS)
	rm *.o

main.o: main.cpp gra.h przeszkoda.h kulka.h pasek.h mysz.h
	$(CC) -c main.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

zegar.o: zegar.cpp
	$(CC) -c zegar.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

gra.o: gra.cpp zegar.h obiekt.h
	$(CC) -c gra.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

przeszkoda.o: przeszkoda.cpp
	$(CC) -c przeszkoda.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

kulka.o: kulka.cpp przeszkoda.h
	$(CC) -c kulka.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

poziom.o: poziom.cpp przeszkoda.h kulka.h
	$(CC) -c poziom.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

pasek.o: pasek.cpp
	$(CC) -c pasek.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

obiekt.o: obiekt.cpp
	$(CC) -c obiekt.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

mysz.o: mysz.cpp
	$(CC) -c mysz.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

renderer.o: renderer.cpp kulka.h
	$(CC) -c renderer.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

plik_gry.o: plik_gry.cpp
	$(CC) -c plik_gry.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

punkt.o: punkt.cpp
	$(CC) -c punkt.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

przycisk.o: przycisk.cpp
	$(CC) -c przycisk.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

nagroda.o: nagroda.cpp
	$(CC) -c nagroda.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

menu.o: menu.cpp
	$(CC) -c menu.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

listaNagrod.o: listaNagrod.cpp
	$(CC) -c listaNagrod.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

clean:
	rm -f *.o gra

cleanAll:
	rm -f *.o *.txt *.bmp gra

#main.test: main.cpp
	#$(CC) -c main.cpp $(INCLUDE) $(CFLAGS) $(LDFLAGS)

#test: main.test renderer.o kulka.o zegar.o przeszkoda.o
	#$(CC) main.o renderer.o kulka.o zegar.o przeszkoda.o -o gra $(INCLUDE) $(CFLAGS) $(LDFLAGS)
