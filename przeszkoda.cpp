#include<allegro5/allegro.h>
#include<allegro5/allegro_image.h>
#include<allegro5/allegro_primitives.h>
#include<math.h>
#include<iostream>
#include "przeszkoda.h"
Przeszkoda::Przeszkoda(double x1, double y1, double x2, double y2, double wspolczynnikOdbicia, int kolor1, int kolor2, int kolor3): skala(0.75){
	this->x1=x1;
	this->x2=x2;
	this->y1=y1;
	this->y2=y2;
	this->wspolczynnikOdbicia=wspolczynnikOdbicia;
	//this->skala=skala;
	this->kolor1=kolor1;
	this->kolor2=kolor2;
	this->kolor3=kolor3;
	double pom1=this->x2-this->x1;
	double pom2=this->y2-this->y1;
	double pom3;
	wektor_normalny_x=-pom2;
	wektor_normalny_y=pom1;
	pom3=sqrt(wektor_normalny_x*wektor_normalny_x+wektor_normalny_y*wektor_normalny_y);
	wektor_normalny_x/=pom3;
	wektor_normalny_y/=pom3;

	//std::cout<<wektor_normalny_x<<" "<<wektor_normalny_y<<" "<<sqrt(wektor_normalny_x*wektor_normalny_x+wektor_normalny_y*wektor_normalny_y)<<std::endl;
}

double Przeszkoda::getX1() const{
	return x1;
}

double Przeszkoda::getX2() const{
	return x2;
}
double Przeszkoda::getY1() const{
	return y1;
}
double Przeszkoda::getY2() const{
	return y2;
}

/*void Przeszkoda::wyswietl() const{
	al_draw_line(x1/getSkala(), y1/getSkala(), x2/getSkala(), y2/getSkala(), al_map_rgba(kolor1, kolor2, kolor3, 128),1);
}*/

double Przeszkoda::getWektorNormalnyY() const{
	return wektor_normalny_y;
}

double Przeszkoda::getWektorNormalnyX() const{
	return wektor_normalny_x;
}

void Przeszkoda::setX1(double a){
	x1=a;
	double pom1=this->x2-this->x1;
	double pom2=this->y2-this->y1;
	double pom3;
	wektor_normalny_x=-pom2;
	wektor_normalny_y=pom1;
	pom3=sqrt(wektor_normalny_x*wektor_normalny_x+wektor_normalny_y*wektor_normalny_y);
	wektor_normalny_x/=pom3;
	wektor_normalny_y/=pom3;
}

void Przeszkoda::setX2(double a){
	x2=a;
	double pom1=this->x2-this->x1;
	double pom2=this->y2-this->y1;
	double pom3;
	wektor_normalny_x=-pom2;
	wektor_normalny_y=pom1;
	pom3=sqrt(wektor_normalny_x*wektor_normalny_x+wektor_normalny_y*wektor_normalny_y);
	wektor_normalny_x/=pom3;
	wektor_normalny_y/=pom3;
}

void Przeszkoda::setY1(double a){
	y1=a;
	double pom1=this->x2-this->x1;
	double pom2=this->y2-this->y1;
	double pom3;
	wektor_normalny_x=-pom2;
	wektor_normalny_y=pom1;
	pom3=sqrt(wektor_normalny_x*wektor_normalny_x+wektor_normalny_y*wektor_normalny_y);
	wektor_normalny_x/=pom3;
	wektor_normalny_y/=pom3;
}
void Przeszkoda::setY2(double a){
	y2=a;
	double pom1=this->x2-this->x1;
	double pom2=this->y2-this->y1;
	double pom3;
	wektor_normalny_x=-pom2;
	wektor_normalny_y=pom1;
	pom3=sqrt(wektor_normalny_x*wektor_normalny_x+wektor_normalny_y*wektor_normalny_y);
	wektor_normalny_x/=pom3;
	wektor_normalny_y/=pom3;
}

double Przeszkoda::getWspolczynnikOdbicia() const{
	return wspolczynnikOdbicia;
}

int Przeszkoda::getKolor1() const{
	return kolor1;
}


int Przeszkoda::getKolor2() const{
	return kolor2;
}

int Przeszkoda::getKolor3() const{
	return kolor3;
}
