//#include "gra.h"
//#include "przeszkoda.h"
#include <iostream>
#include<functional>
#include<allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include "kulka.h"
#include<ctime>
#include"pasek.h"
#include "mysz.h"
#include "renderer.h"
#include "gra.h"
#include "plik_gry.h"
#include "listaNagrod.h"

/*void test_pasek(){
	Pasek pasek_(0, 0, 1.0, 0.4);
	Gra::getGra();
	double a=0;
	while(!al_key_down(Gra::getGra().getKlawiatura(), ALLEGRO_KEY_ESCAPE)){
		pasek_.wyswietl(a);
		a=a+0.5;
		if(a==100.0)a=0;
		Gra::getGra().wyswietl();
		Gra::getGra().odczytKlawiatury();
		al_rest(0.01);
	}

}*/

void test_silnika(){
	//srand(time(NULL));
	Gra::getGra();
	al_rest(1.0);
	//Gra::getGra().generujLosowePrzeszkody();
	//Gra::getGra().wyswietl();
	//Gra::getGra().inicjujPoziom();
	std::cout<<"okk*\n";
	Gra::getGra().dodajPrzeszkody();
	//Gra::getGra().wyswietl();
	std::cout<<"okkkkkkkkkkkkkk";
	//test_pasek();
	/*while(!al_key_down(Gra::getGra().getKlawiatura(), ALLEGRO_KEY_ESCAPE)){
		Gra::getGra().wyswietl();
		Gra::getGra().cyklGry();
		Gra::getGra().odczytKlawiatury();
	}*/
}

/*void test_myszy(){
	Gra::getGra();
	Mysz mysz;
	int poprzedni=0;
	int stan;
	while(!al_key_down(Gra::getGra().getKlawiatura(), ALLEGRO_KEY_ESCAPE)){
		stan=mysz.getButton();
		if(poprzedni==0&&stan)mysz.zaczep();
		if(stan){
			mysz.wyswietlLinie();
			Gra::getGra().wyswietl();
		}
		Gra::getGra().odczytKlawiatury();
		al_rest(0.01);
		poprzedni=stan;
	}

}*/

void test_renderera(){
	al_init();
	al_install_mouse();
	al_install_keyboard();
	al_init_image_addon();
	al_init_primitives_addon();
	Renderer render;
	Kulka kulka;
	Przeszkoda przeszkoda=Przeszkoda(0.1, 0.2, 0.3, 0.4);
	render.wyswietl(&kulka);
	render.wyswietl(&przeszkoda);
	render.wyswietlEkran();
	al_rest(5.0);
}

void test_plikow(){
	PlikGryDoZapisu kontroler;
	std::cout<<kontroler.getNazwa()<<std::endl;
	std::cout<<kontroler.isEmpty()<<std::endl;
	kontroler.nowyPlik();
	kontroler<<8.0;
	kontroler.zamknijPlik();
	kontroler.zamknijPlik();
	kontroler.zamknijPlik();
	kontroler<<9.0;
	kontroler.nowyPlik();
	kontroler.nowyPlik();
	kontroler<<10.0;
	kontroler.zamknijPlik();
	kontroler<<11.0;
}

void test_menu(){
	Gra::getGra();
	al_rest(1.0);
	Gra::getGra().wlaczMenu();
}

void test_rozgrywki(){
	Gra::getGra().rozgrywka();
}

void test_listy(){
	ListaNagrod a("pliki/nagrody.db", 1);
	ListaNagrod b("pliki/nagrody.db", 1);
	std::vector<std::reference_wrapper<int>> dupa;
}

int main(){
	//test_plikow();
	//Gra::getGra();
	//test_menu();
	//test_silnika();
	//test_myszy();
	//test_pasek();
	//test_renderera();
	//test_listy();
	test_rozgrywki();
	return 0;
}
