#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <math.h>
#include <iostream>
#include "kulka.h"
//#include "przeszkoda.h"
#include "zegar.h"
Kulka::Kulka(double x, double y, double promien, int kolor1, int kolor2, int kolor3) :skala(0.75){
	this->x=x;
	this->y=y;
	x_poprzednia=this->x;
	y_poprzednia=this->y;
	this->promien=promien;
	wsp_odbicia=0.7;
	szybkosc_x=0.01;
	szybkosc_y=0.01;
	przyspieszenie=1.0;
	this->kolor1=kolor1;
	this->kolor2=kolor2;
	this->kolor3=kolor3;
	tempMnoznik=0;
	//this->skala=skala;
	miernik.rozpocznij();
}

void Kulka::przesun(){
	double mnoznik=(double)(miernik.czasTrwaniaMilis()/1000000.0);
	tempMnoznik=mnoznik;
	miernik.rozpocznij();
	szybkosc_y+=przyspieszenie*mnoznik;
	x_poprzednia=x;
	y_poprzednia=y;
	x+=szybkosc_x*mnoznik;
	y+=szybkosc_y*mnoznik;
	//szybkosc_y+=przyspieszenie*mnoznik;
}

double Kulka::getSzybkoscX() const{
	return szybkosc_x;
}

double Kulka::getSzybkoscY() const{
	return szybkosc_y;
}

void Kulka::setSzybkoscX(double a){
	szybkosc_x=a;
}

void Kulka::setSzybkoscY(double a){
	szybkosc_y=a;
}

/*void Kulka::wyswietl() const{
	al_draw_filled_circle(x/getSkala(), y/getSkala(), promien/getSkala(), al_map_rgba(kolor1, kolor2, kolor3, 128));
}*/

/*void Kulka::startuj(){
	miernik.rozpocznij();
}*/

double Kulka::getX() const{
	return x;
}

double Kulka::getY() const{
	return y;
}

double Kulka::getR() const{
	return promien;
}

/*double Kulka::getWektorNormalnyX(){
	return wektor_normalny_x;
}

double Kulka::getWektorNormalnyY(){
	return wektor_normalny_y;
}*/

void Kulka::startuj(double vx, double vy){
	szybkosc_x=vx;
	szybkosc_y=vy;
	active=1;
	miernik.rozpocznij();
}

bool Kulka::aktywny() const{
	return active;
}

void Kulka::odbij(int sciana){
	if(sciana==0||sciana==2){
	szybkosc_x=szybkosc_x*wsp_odbicia;
	szybkosc_y=-szybkosc_y*wsp_odbicia;
}
	else{
		szybkosc_x=-szybkosc_x*wsp_odbicia;
		szybkosc_y=szybkosc_y*wsp_odbicia;
	}
}

void Kulka::odbij(Przeszkoda &przeszkoda){
	double iloczyn_skalarny=szybkosc_x*przeszkoda.getWektorNormalnyX()+szybkosc_y*przeszkoda.getWektorNormalnyY();
	//std::cout<<iloczyn_skalarny<<" "<<przeszkoda.getWektorNormalnyX()<<" "<<szybkosc_x*przeszkoda.getWektorNormalnyX()<<std::endl;
	//std::cin>>iloczyn_skalarny;
	szybkosc_x-=2.0*przeszkoda.getWektorNormalnyX()*iloczyn_skalarny;
	szybkosc_y-=2.0*przeszkoda.getWektorNormalnyY()*iloczyn_skalarny;
	szybkosc_x*=(wsp_odbicia*przeszkoda.getWspolczynnikOdbicia());
	szybkosc_y*=(wsp_odbicia*przeszkoda.getWspolczynnikOdbicia());
}

double Kulka::getTempMnoznik() const{
	return tempMnoznik;
}

void Kulka::ustawPoprzednieWspolrzedne(){
	x=x_poprzednia;
	y=y_poprzednia;
}

void Kulka::resetPoprzednie(){
	x_poprzednia=x;
	y_poprzednia=y;
}

double Kulka::getOdleglosc(const Punkt& a) const{
	auto kwadrat=[](double &&x) ->double {return x*x;};
	return sqrt(kwadrat(a.getX()-getX())+kwadrat(a.getY()-getY()));
}

void Kulka::setX(double a){
	x=a;
}

void Kulka::setY(double a){
	y=a;
}

void Kulka::stop(){
	szybkosc_x=0;
	szybkosc_y=0;
}

void Kulka::centruj(){
	x=0.5;
	y=skala*0.5;
}
