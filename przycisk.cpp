#include "przycisk.h"

template <class T>
Przycisk<T>::Przycisk(const T &a) :opis(std::to_string(a)){
	x1=x2=y1=y2=0.0;
	rozwiniecie="dupa";
}

/*template <class T>
Przycisk<T>::Przycisk(const std::string &a):opis(a){
	x1=x2=y1=y2=0;
}*/

template <>
Przycisk<std::string>::Przycisk(const std::string &a):opis(a){
	x1=x2=y1=y2=0;
	rozwiniecie="dupa";
}

template <class T>
const std::string Przycisk<T>:: getRozwiniecie() const{
	return rozwiniecie;
}

template <class T>
double Przycisk<T>::getX1() const{
	return x1;
}

template <class T>
double Przycisk<T>::getX2() const{
	return x2;
}

template <class T>
double Przycisk<T>::getY1() const{
	return y1;
}

template <class T>
double Przycisk<T>::getY2() const{
	return y2;
}

template <class T>
std::string Przycisk<T>::getOpis() const{
	return opis;
}

template <class T>
void Przycisk<T>::setX1(double a){
	x1=a;
}

template <class T>
void Przycisk<T>::setX2(double a){
	x2=a;
}

template <class T>
void Przycisk<T>::setY1(double a){
	y1=a;
}

template <class T>
void Przycisk<T>::setY2(double a){
	y2=a;
}
template <class T>
void Przycisk<T>::setRozwiniece(const std::string &rozwiniecie){
	this->rozwiniecie=rozwiniecie;
}

template <class T>
Przycisk<T>::Przycisk(){
	opis="";
	rozwiniecie="dupa";
	x1=x2=y1=y2=0;
}

template <class T>
void Przycisk<T>::setOpis(const T &a){
	opis=std::to_string(a);
}

template <>
void Przycisk <std::string>::setOpis(const std::string &a){
	opis=a;
	//rozwiniecie="dupa";
}

template class Przycisk <std::string>;
template class Przycisk <int>;
