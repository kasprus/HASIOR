#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include "mysz.h"
#include <iostream>
Mysz::Mysz(int x, int y){
	al_install_mouse();
	xZaczepienia=0;
	yZaczepienia=0;
	skala=1.0/double(x);
}


Mysz::~Mysz(){
	al_uninstall_mouse();
}

double Mysz::getX(){
	al_get_mouse_state(&stanMyszy);
	return double(stanMyszy.x)*skala;
}

double Mysz::getY(){
	al_get_mouse_state(&stanMyszy);
	return double(stanMyszy.y)*skala;
}

double Mysz::getButton(){
	al_get_mouse_state(&stanMyszy);
	return stanMyszy.buttons;
}

void Mysz::zaczep(){
	al_get_mouse_state(&stanMyszy);
	xZaczepienia=double(stanMyszy.x)*skala;
	yZaczepienia=double(stanMyszy.y)*skala;
}

/*void Mysz::wyswietlLinie(){
	std::cout<<xZaczepienia/getSkala()<<" "<<getX()/getSkala()<<"\n";
	al_draw_line(xZaczepienia/getSkala(), yZaczepienia/getSkala(), getX()/getSkala(), getY()/getSkala(), al_map_rgb(200, 20, 40), 1);
}*/

double Mysz::getZaczepienieX() const{
	return xZaczepienia;
}

double Mysz::getZaczepienieY() const{
	return yZaczepienia;
}
