#ifndef PLIK_GRY_
#define PLIK_GRY_

#include <iostream>
#include <fstream>
#include <functional>
#include <list>
#include <vector>
#include "przeszkoda.h"
#include "poziom.h"

class PlikGry{
protected:
	std::string nazwaOgolna;
	const std::string rozszerzenie;
	const std::string folder;
	unsigned int numerPliku;
	unsigned int aktualny;
	struct Pole{
		std::fstream plik;
		Pole *next;
	};
	Pole *poczatek;
	Pole *koniec;

public:
	PlikGry(std::string nazwa="mapa");
	virtual ~PlikGry();
	std::string getNazwa() const;
	//virtual void czytajPoziom(std::list<Przeszkoda*> &listaDocelowa)=0;
	//virtual void nowyPlik()=0;
	//virtual void przeszukajPliki()=0;
	void zamknijPlik();
	bool isEmpty() const;
	std::string getAktualnaNazwa() const;
};



class PlikGryDoZapisu :public PlikGry{
public:
	PlikGryDoZapisu(std::string nazwa="mapa");
	~PlikGryDoZapisu();
	void nowyPlik();
	PlikGryDoZapisu & operator<<(double);
	//bool operator<<(std::string);
};


class PlikGryDoOdczytu: public PlikGry{
public:
	PlikGryDoOdczytu(std::string nazwa="mapa");
	~PlikGryDoOdczytu();
	void otworzPliki(int nr);
	void ladujPoziomy(std::vector<std::reference_wrapper<Poziom>> &zbior);
};

#endif
