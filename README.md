#HASIOR
##Hasior Game

Celem gry jest skierowanie kulki do punktu docelowego. W tym celu gracz myszką nadaje kulce prędkość początkową i kierunek. Kulka odbija się od ścian i przeszkód.

---

Istnieją trzy rodzaje przeszkód:

* zwykłe
* tłumiące
* przyspieszające
